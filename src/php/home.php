<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Index -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="icon" type="image/png" href="../assets/logo/picto-e.png"/>

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

    <header class="topnav" id="Navbar">
      <a href="home.php" class="logo" data-aos="fade-right" data-aos-duration="900"><img id="logo" src="../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
      <a href="javascript:void(0);" class="icon" onclick="openNav()">
        <i class="fa fa-bars"></i>
      </a>
      <a href="contact.php" class="item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
      <a href="about.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
      <a href="./blog/how-to-create-a-brand.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
      <a href="./whatwedo/marketing.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
      <a href="./whatwedo/logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
      <a href="./whatwedo/selling.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
    </header>

    <?php require_once './partials/menu-js.php'; ?>

    <!-- Entrance -->
    <section class="wrapper" id="entrance">
      <div class="entrance-img o-40 cover" data-aos="fade-right" data-aos-duration="800"></div>
      <div class="o-60 entrance-txt relative">
        <div class="white-bg absolute"></div>
        <div class="entrance-txt-content">
          <h1 data-aos="fade-left" data-aos-duration="800" data-aos-delay="200" data-aos-anchor="#entrance">Your brand<br> cherished in France</h1>
          <p class="black-txt fix-lh-txt" data-aos="fade-left" data-aos-duration="800" data-aos-delay="300">We love cosmetics as you love your brand.
          We believe in you, we build our way on trust
          and care to <span class="red-txt">help small companies to export</span>
          their beloved products in France.</p>
          <!-- <button data-aos="fade-left" data-aos-duration="800" data-aos-delay="400" data-aos-anchor-placement="top-bottom"><a href="contact.php">Discover our way</a></button> -->
          <a class="a-button" data-aos="fade-left" data-aos-duration="800" data-aos-delay="400" data-aos-anchor-placement="top-bottom" href="contact.php">Discover our way</a>
        </div>
      </div>
    </section>

    <!-- About -->
    <section class="wrapper about-link mb-5">
      <div class="codebar codebar-entrance"></div>
      <div class="o-two-thirds" data-aos="fade-right" data-aos-duration="1000">
        <div>
          <h2 class="black-txt">Let your <br>brand grow,<br>
          you can <br class="hide-desktop">count <br>on us</h2>
          <!-- <button><a class="black-txt" href="contact.php">Capture who we are</a></button> -->
          <a class="a-button-black hide-mobile" href="contact.php">Capture who we are</a>
        </div>
      </div>
      <div class="o-third img-dancing cover" data-aos="fade-left" data-aos-duration="1000"></div>
    </section>

    <!-- This is what we do -->
    <div class="o-container blue-bg pt-5">
      <h2 class="red-txt txt-center mt-5" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="250" >This is <br class="hide-desktop">what we do</h2>
    </div>

    <section class="o-container wrapper py-5 blue-bg">
      <!-- Wrapper -->
      <div class="wrapper-start mt-3">
        <!-- Selling -->
        <div class="o-third" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="450">
          <div class="vertical-middle">
            <h3>1. Selling <!--<img src="../assets/icons/picto_selling.png" alt=""> --></h3>
            <p class="dark-txt fix-lh-txt mt-3 mb-4">
              We elaborate your <strong class="red-txt">sale strategy</strong> perfectly fitted for France’s sellers and customers.
              We are your custom <strong class="red-txt">prospectors</strong>
              and your own <strong class="red-txt"> sales agents</strong>
              <br class="hide-mobile">in France.
            </p>
            <!-- <button><a href="./whatwedo/selling.php">Learn more</a></button> -->
            <a class="a-button d-inline-block" href="./whatwedo/selling.php">Learn more</a>
          </div>
        </div>
        <!-- Logistic -->
        <div class="o-third" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="600">
          <div class="vertical-middle">
            <h3>2. Logistic <!--<img src="../assets/icons/picto_logistic.png" alt=""> --></h3>
            <p class="dark-txt fix-lh-txt mt-3 mb-4">
              Sell peacefully and more effectively in France
              while <strong class="red-txt">keeping control</strong>
              of your <strong class="red-txt">expeditions and costs</strong>.
              <br class="hide-mobile">You entrust us with your products,
              we take care of everything else.
            </p>
            <!-- <button><a href="./whatwedo/logistic.php">Learn more</a></button> -->
            <a class="a-button d-inline-block" href="./whatwedo/logistic.php">Learn more</a>
          </div>
        </div>
        <!-- Marketing -->
        <div class="o-third" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="750">
          <div class="vertical-middle">
            <h3>3. Marketing <!--<img src="../assets/icons/picto_marketing.png" alt=""> --></h3>
            <p class="dark-txt fix-lh-txt mt-3 mb-4">
              Produce a <strong class="red-txt">strong effect</strong>
              <br class="hide-mobile">in <strong class="red-txt">the right mind</strong>.
              We can polish your brand image and improve your presence on <strong class="red-txt">social media</strong>.
            </p>
            <!-- <button><a href="./whatwedo/marketing.php">Learn more</a></button> -->
            <a class="a-button d-inline-block" href="./whatwedo/marketing.php">Learn more</a>
          </div>
        </div>
      </div>
    </section>

    <!-- Quote -->
    <section class="o-container wrapper pt-5 p blue-bg" id="quote">
      <!-- Text part -->
      <div class="o-half" data-aos="fade-down">
          <blockquote cite="#" class="hide-mobile o-half">
            <header>"An incredible<br>adventure"</header>
            <p class="w-100 dark-txt fix-lh-txt my-3">Our job as marketers is to understand how the customer wants to buy and help them do it.</p>
            <cite class="red-txt">Bryan Eisenberg<br>Online marketing pioneer</cite>
          </blockquote>
      </div>
      <!-- Img part -->
      <div class="o-third" data-aos="fade-left" data-aos-duration="2000">
        <img class="img-quote" src="../assets/img/3.jpg">
      </div>
    </section>

    <!-- Partners -->
    <section class="section-partners white-bg">
      <div class="o-container">
        <h1 class="txt-center-fixed w-100" data-aos="slide-up" data-aos-delay="300" data-aos-anchor-placement="bottom-bottom">They gifted us their trust</h1>
        <div class="wrapper img-deck img-deck-home aosSitch" data-aos="flip-up" data-aos-delay="300" data-aos-anchor-placement="bottom-bottom">
          <a href="https://staramydlarnia.sklep.pl/" target="_blank"><img src="../assets/partners/bodymania.png" alt="Bodymania partner"></a>
          <a href="https://en.farmersbeautymarket.com/" target="_blank"><img src="../assets/partners/superfood.png" alt="Superfood For Skin partner"></a>
          <a href="http://www.pausecosmetics.com/" target="_blank"><img src="../assets/partners/pause.png" alt="Pause partner"></a>
          <a href="https://www.barwa.com.pl/" target="_blank"><img src="../assets/partners/barwa.png" alt="Barwa partner"></a>
          <a href="http://paesecosmetics.in/about-us/" target="_blank"><img src="../assets/partners/paese.png" alt="Paesepartner"></a>
        </div>
      </div>
    </section>

    <!-- Images -->
    <section class="section-img-divider wrapper">
      <div class="img-4 cover o-half" data-aos="fade-up-right" data-aos-duration="1500"></div>
      <div class="img-5 cover o-half" data-aos="fade-up-left" data-aos-duration="1500"></div>
    </section>

    <!-- Join us -->
    <section class="join-us-section shadow-3-bg">
      <baseline class=" black-txt">MAKE MORE POSSIBLE.</baseline>
      <div class="img-6 cover">
        <div class="wrapper join-us-content">
          <div class="h1 red-txt" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-delay="300" data-aos-duration="1000">Join us</div>
          <div class="border red-bg hide-mobile" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-delay="450" data-aos-duration="1000"></div>
          <div class="h1 red-txt" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-delay="600" data-aos-duration="1000">on an <br class="hide-desktop">adventure</div>
        </div>
      </div>
      <!-- <button><a href="contact.php">Build our partnership</a></button> -->
      <div class="wrapper mt-5">
        <a class="a-button" href="contact.php">Build our partnership</a>
      </div>
    </section>

    <?php require_once './partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>

</body>

<?php require_once './partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../js/scripts.js"></script>

</html>
