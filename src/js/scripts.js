( function( $ ) {

  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $( 'a.js-scroll-trigger[href*="#"]:not([href="#"])' ).click( function() {
    if ( location.pathname.replace( /^\//, '' ) == this.pathname.replace( /^\//, '' ) && location.hostname == this.hostname ) {
      var target = $( this.hash );
      target = target.length ? target : $( '[name=' + this.hash.slice( 1 ) + ']' );
      if ( target.length ) {
        $( 'html, body' ).animate( {
          scrollTop: ( target.offset().top - 72 )
        }, 1000, "easeInOutExpo" );
        return false;
      }
    }
  } );

  // Closes responsive menu when a scroll trigger link is clicked
  $( '.js-scroll-trigger' ).click( function() {
    $( '.topnav' ).collapse( 'responsive' );
  } );


  // --------------------------------------------------------------------------------------------------------------------------------------

  // Activate scrollspy to add active class to navbar items on scroll
  $( 'body' ).scrollspy( {
    target: '#Navbar',
    offset: 75
  } );

  // Collapse Navbar
  var navbarCollapse = function() {
    // SI l'ecran est grand
    if ( window.matchMedia( "(min-width: 60px)" ).matches ) {
      // Dès que l'on scroll
      if ( $( "#Navbar" ).offset().top > 75 ) {
        // La barre devient blanche
        $( "#Navbar" ).addClass( "white-bg" );
        $( "#Navbar" ).addClass( "shadow-sm" );
        $( "#anchor" ).css( "display", "block" );
        document.getElementById( "logo" ).style.transition = "all 1s";
      } else {
        // Si on reste de la section d'accueil
        $( "#Navbar" ).removeClass( "white-bg" );
        $( "#Navbar" ).removeClass( "shadow-sm" );
        $( "#anchor" ).css( "display", "none" );
        document.getElementById( "logo" ).style.transition = "all 1s";
      }

      // var bottom = $( '.blog--menu' ).position().top + $( '.blog--menu' ).height();
      // console.log( bottom );
      //
      // while ( bottom > 200 ) {
      //   $( ".blog--menu" ).css( "display", "none" );
      // } else {
      //   $( ".blog--menu" ).css( "display", "block" );
      // }
    };
  }
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $( window ).scroll( navbarCollapse );

  // --------------------------------------------------------------------------------------------------------------------------------------

  function toResponsiveDesign( x ) {
    if ( x.matches ) { // If media query matches

      // Changes in AOS
      $( ".aosSitch" ).attr( {
        "data-aos": "slide-up",
        "data-aos-anchor-placement": "top-center",
        "data-aos-duration": "800"
      } );

      // Changes width of elements
      $( ".width50to100" ).removeClass( "w-50" );
      $( ".width50to100" ).addClass( "w-100" );
      $( ".width68to100" ).removeClass( "w-68" );
      $( ".width68to100" ).addClass( "w-100" );

      $( ".width100to75" ).removeClass( "w-50" );
      $( ".width100to75" ).addClass( "w-100" );


      // Changes margin
      $( "#quote" ).removeClass( "pt-5" );
      $( ".about3" ).removeClass( "pt-5" );

    } else {

      // Changes in AOS
      $( ".aosSitch" ).attr( {
        "href": "flip-up",
        "title": "bottom-bottom",
        "data-aos-duration": "500"
      } );

      // Changes width of elements
      $( ".width50to100" ).removeClass( "w-100" );
      $( ".width50to100" ).addClass( "w-50" );
      $( ".width68to100" ).removeClass( "w-100" );
      $( ".width68to100" ).addClass( "w-68" );

      $( ".width100to75" ).removeClass( "w-100" );
      $( ".width100to75" ).addClass( "w-50" );

      // Changes margin
      $( "#quote" ).addClass( "pt-5" );
      $( ".about3" ).addClass( "pt-5" );

    }
  }

  let x = window.matchMedia( "(max-width: 768px)" )

  toResponsiveDesign( x ) // Call listener function at run time
  x.addListener( toResponsiveDesign ) // Attach listener function on state changes

  $( ".blog--hook" ).click( function() {
    console.log( "Handler for .click() called." );
    $( this ).find( ".fas" ).toggleClass( "fa-arrow-circle-right" );
    $( this ).find( ".fas" ).toggleClass( "fa-times-circle" );
    $( this ).next().toggleClass( "blog--fadeIn" );
    $( this ).next().toggleClass( "blog--fadeOut" );
  } );

} )( jQuery ); // End of use strict
