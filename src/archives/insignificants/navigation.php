<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/design-system.css">
<link rel="stylesheet" href="style.css">
<script src="app.js"></script>

<nav>
    <div class="wrapper haut">
        <div class="menu"><img src="../img/menu.png" alt=""></div>
        <div class="logo"><img src="../images/picto-easyd.png" alt=""></div>

        <ul class="wrapper millieu">
            <li><a href="http://localhost/integration/index.php">Home</a></li>
            <li class="wrapper red"><a href="">What we do</a><img src="/images/fleche.png" alt=""> </li>
            <li><a href="http://localhost/integration/contact.html">About us</a></li>
        </ul>

        <ul class="button">
            <li><button><a href="/contact.html">Contact us</a></button></li>
        </ul>

    </div>

    <ul class="wrapper bas">
        <li class="wrapper"><img src="/images/picto_selling.png" alt=""><a href="">Selling</a></li>
        <li class="wrapper"><img src="/images/picto_logistic.png" alt=""><a href="">Logistic</a></li>
        <li class="wrapper"><img src="/images/picto_marketing.png" alt=""><a href="">Marketing</a></li>
    </ul>
</nav>
