<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- About -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="icon" type="image/png" href="../assets/logo/picto-e.png"/>

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

    <header class="topnav" id="Navbar">
      <a href="home.php" class="logo"><img id="logo" src="../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
      <a href="javascript:void(0);" class="icon" onclick="openNav()">
        <i class="fa fa-bars"></i>
      </a>
      <a href="contact.php" class="item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
      <a href="about.php" class="active item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
      <a href="./blog/how-to-create-a-brand.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
      <a href="./whatwedo/marketing.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
      <a href="./whatwedo/logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
      <a href="./whatwedo/selling.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
    </header>

    <?php require_once './partials/menu-js.php'; ?>

    <main>
      <!-- First -->
      <section class="o-container about d-block">
        <div class="o-half">
          <baseline class="black-txt">ABOUT US.</baseline>
          <h2 class="red-txt mt-2"> A unique agency dedicated to help<br>you export your brand.</h2>
          <p class="black-txt fix-lh-txt mt-3 mb-5"> Easy Distribution has been created<br class="hide-mobile"> in 2015 in Toulouse,
            France by three brothers passionate by care and cosmetic products.</p>
          </div>
          <img class="ml-5 img10 mobile-mt-5" src="../assets/img/10.jpg">
        </section>


        <!-- Second -->
        <section class="o-container wrapper blue-bg pb-5">
          <div class="o-half" data-aos="zoom-in" data-aos-duration="1000">
            <img class="img11 mb-3 mt-lg-n5" src="../assets/img/11.jpg">
            <h3 class="red-txt">Gather,<br> understand,<br> and grow</h3>
            <p class="dark-txt my-3 w-68 width68to100">Our strength is our empathy. We understand your needs and build everything possible to achieve your goals and nurture your company. <br class="hide-mobile">We provide you all our knowledge and skills to help you open to the world.</p>
            <!-- <a href="./whatwedo/selling.php" class="red-txt">Discover our selling prestations</a> -->
            <a class="a-button d-inline-block mt-3" href="./whatwedo/selling.php">Discover our selling prestations</a>
          </div>
          <div class="o-half mobile-mt-5" data-aos="zoom-out" data-aos-duration="1000" data-aos-offset="200">
            <p class="red-txt"><strong>WE ARE DREAMERS,<br>WE MAKE MORE POSSIBLE</strong></p>
            <img class="img12 my-3" src="../assets/img/12.jpg">
            <p class="dark-txt mt-2 ml-5"><strong>IT'S A STORY ABOUT BEGINNING WITH NOTHING, <br class="hide-mobile"> LEARNING AND SHARING IT WITH THE WORLD.</strong></p>
          </div>
        </section>
        <div class="barre_mip center hide-mobile">
        </div>

        <!-- Third -->
        <section class="about3 pt-5 blue-bg">
          <div class="div1" data-aos="fade-right" data-aos-duration="1000">
            <h2 class="white-txt" data-aos="flip-right" data-aos-delay="500" data-aos-duration="1000">Make it</h2>
          </div>
          <div class="div2" data-aos="fade-left" data-aos-duration="1000">
            <h2 class="white-txt" data-aos="flip-right" data-aos-delay="700" data-aos-duration="1000">possible</h2>
          </div>
        </section>
        <div class="hide-mobile">
        </div>

        <!-- Partners -->
        <section class="section-partners white-bg mt-n5">
          <div class="txt-center-fixed w-100">
            <div data-aos="slide-up" data-aos-duration="800">
              <h5 class="black-txt mt-5">OUR PARTNERS.</h5>
              <h2 class="red-txt my-3">Friends we've met <br class="hide-mobile"> on our journey</h2>
            </div>
            <div class="wrapper img-deck img-deck-about">
              <a href="https://www.beauteprivee.fr/" data-aos="flip-up" data-aos-duration="800" target="_blank"><img src="../assets/partners/beauteprivee.png" alt="beaute privee"></a>
              <a href="https://www.my-store.ch/fr/" data-aos="flip-up" data-aos-duration="800" target="_blank"><img src="../assets/partners/mystorech.png" alt="mystore.ch"></a>
              <a href="https://www.brandalley.fr/" data-aos="flip-up" data-aos-duration="800" target="_blank"><img src="../assets/partners/brandalley2.png" alt="brand alley"></a>
              <a href="https://www.showroomprive.com/" data-aos="flip-up" data-aos-duration="800" target="_blank"><img src="../assets/partners/showroom2.png" alt="Barwa partner"></a>
            </div>
          </div>
        </section>

        <!-- Contact -->
        <div class="pb-5 txt-center-fixed w-100" data-aos="fade-up" data-aos-duration="800">
          <h5 class="black-txt mt-5">STAND OUT FROM THE CROWD.</h5>
          <h2 class="red-txt my-3"> Be a part of family</h2>
          <a class="red-txt" href="contact.php"><strong> Contact us now</strong> </a>
        </div>

    </main>


    <?php require_once './partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>

</body>

<?php require_once './partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../js/scripts.js"></script>

</html>
