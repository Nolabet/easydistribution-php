<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Index -->
    <title>Easy Distribution</title>

    <!-- Styles -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- <link rel="stylesheet" href="../css/test/test.css">
    <link rel="stylesheet" href="../css/test/style-test.css"> -->
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/design-system.css">
    <link rel="icon" type="image/png" href="../assets/logo/picto-e.png"/>

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

    <!-- Header -->
    <!-- <header class="fixed zindex">
        <div>
            <nav>
              <div class="w-100" id="Navbar">
                <a href="home.php" class="logo ml-5 mr-2"><img id="logo" src="../assets/logo/picto-easyd-red.svg" alt=""></a>
                <button type="button" name="button" class="burger m-4 mr-5"><img src="../assets/icons/menu.svg"alt=""></button>
                <ul class="menu pt-2">
                    <li><a href="./whatwedo/selling.php" class="pl-2" data-aos="fade-left" data-aos-duration="1000">> Selling</a></li>
                    <li><a href="./whatwedo/logistic.php" class="pl-2" data-aos="fade-left" data-aos-duration="1050">> Logistic</a></li>
                    <li><a href="./whatwedo/marketing.php" class="pl-2" data-aos="fade-left" data-aos-duration="1100">> Marketing</a></li>
                    <li><a href="about.php" class="pl-2 black-txt" data-aos="fade-left" data-aos-duration="1150">> About</a></li>
                    <button id="contact" class="mr-5" data-aos="fade-left" data-aos-duration="1200"><a href="contact.php" class="red-txt">Contact us</a></button>
                </ul>
              </div>
            </nav>
        </div>
    </header> -->

    <script>
      AOS.init();
    </script>

    <!-- Entrance -->
    <section class="wrapper">
      <div class="entrance-img o-half cover"></div>
      <div class="o-half entrance-txt relative">
        <div class="white-bg absolute"></div>
        <div class="entrance-txt-content">
          <h1>Your brand<br> cherished in France</h1>
          <p class="black-txt fix-lh-txt">We love cosmetics as you love your brand.
          We believe in you, we build our way on trust
          and care to <span class="red-txt">help small companies to export</span>
          their beloved products in France.</p>
          <button><a href="contact.php">Discover our way</a></button>
        </div>
      </div>
    </section>

    <!-- About -->
    <section class="wrapper about-link my-5">
      <div class="codebar codebar-entrance"></div>
      <div class="o-two-thirds" data-aos="fade-right" data-aos-duration="1000">
        <div>
          <h2 class="black-txt">Let your <br>brand grow,<br>
          you can count <br>on us</h2>
          <button><a class="black-txt" href="contact.php">Capture who we are</a></button>
        </div>
      </div>
      <div class="o-third img-dancing cover" data-aos="fade-left" data-aos-duration="1000"></div>
    </section>

    <!-- This is what we do -->
    <div class="o-container blue-bg pt-5">
      <h2 class="red-txt txt-center mt-5" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="250" >This is what <br class="hide-desktop">we do</h2>
    </div>

    <section class="o-container wrapper py-5 blue-bg">
      <!-- Wrapper -->
      <div class="wrapper-start">
        <!-- Selling -->
        <div class="o-third mt-5" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="450">
          <div class="vertical-middle">
            <h3>1. Selling <!--<img src="../assets/icons/picto_selling.png" alt=""> --></h3>
            <p class="w-68 dark-txt fix-lh-txt my-3">
              We elaborate your <strong class="red-txt">sale strategy</strong> perfectly fitted for France’s sellers and customers.<br>
              We are your custom <strong class="red-txt">prospectors</strong>
              and your own <strong class="red-txt">sales agents</strong><br>
              in France.
            </p>
            <button><a href="./whatwedo/selling.php">Learn more</a></button>
          </div>
        </div>
        <!-- Logistic -->
        <div class="o-third mt-5" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="600">
          <div class="vertical-middle">
            <h3>2. Logistic <!--<img src="../assets/icons/picto_logistic.png" alt=""> --></h3>
            <p class="w-68 dark-txt fix-lh-txt my-3">
              Sell peacefully and more effectively in France
              while <strong class="red-txt">keeping control</strong>
              of your <strong class="red-txt">expeditions and costs</strong>.
              You entrust us with your products,<br>
              we take care of everything else.
            </p>
            <button><a href="./whatwedo/logistic.php">Learn more</a></button>
          </div>
        </div>
        <!-- Marketing -->
        <div class="o-third mt-5" data-aos="fade-down" data-aos-duration="1500" data-aos-delay="750">
          <div class="vertical-middle">
            <h3>3. Marketing <!--<img src="../assets/icons/picto_marketing.png" alt=""> --></h3>
            <p class="w-70 dark-txt fix-lh-txt my-3">
              Produce a <strong class="red-txt">strong effect</strong>
              in <strong class="red-txt">the right mind</strong>.
              We can polish your brand image
              and improve <br> your presence on<br><strong class="red-txt">social media</strong>.
            </p>
            <button><a href="./whatwedo/marketing.php">Learn more</a></button>
          </div>
        </div>
      </div>
    </section>

    <!-- Quote -->
    <section class="o-container wrapper pt-5 p blue-bg">
      <!-- Text part -->
      <div class="o-half" data-aos="fade-down">
          <blockquote cite="#" class="hide-mobile o-half">
            <header>"An incredible<br>adventure"</header>
            <p class="w-100 dark-txt fix-lh-txt my-3">Our job as marketers is to understand how the customer wants to buy and help them do it.</p>
            <cite class="red-txt">Bryan Eisenberg<br>Online marketing pioneer</cite>
          </blockquote>
      </div>
      <!-- Img part -->
      <div class="o-third" data-aos="fade-left" data-aos-duration="2000">
        <img class="img-quote mt-lg-5 mb-lg-n5" src="../assets/img/3.jpg">
      </div>
    </section>

    <!-- Partners -->
    <section class="section-partners white-bg">
      <div class="o-container">
        <h1 class="txt-center" data-aos="slide-up" data-aos-delay="300" data-aos-anchor-placement="bottom-bottom">They gifted us their trust</h1>
        <div class="wrapper img-deck img-deck-home" data-aos="flip-up" data-aos-delay="300" data-aos-anchor-placement="bottom-bottom">
          <a href="https://staramydlarnia.sklep.pl/" target="_blank"><img src="../assets/partners/bodymania.png" alt="Bodymania partner"></a>
          <a href="https://en.farmersbeautymarket.com/" target="_blank"><img src="../assets/partners/superfood.png" alt="Superfood For Skin partner"></a>
          <a href="http://www.pausecosmetics.com/" target="_blank"><img src="../assets/partners/pause.png" alt="Pause partner"></a>
          <a href="https://www.barwa.com.pl/" target="_blank"><img src="../assets/partners/barwa.png" alt="Barwa partner"></a>
          <a href="http://paesecosmetics.in/about-us/" target="_blank"><img src="../assets/partners/paese.png" alt="Paesepartner"></a>
        </div>
      </div>
    </section>

    <!-- Images -->
    <section class="section-img-divider wrapper">
      <div class="img-4 cover o-half" data-aos="fade-up-right" data-aos-duration="1500"></div>
      <div class="img-5 cover o-half" data-aos="fade-up-left" data-aos-duration="1500"></div>
    </section>

    <!-- Join us -->
    <section class="join-us-section shadow-3-bg">
      <baseline class=" black-txt">MAKE MORE POSSIBLE.</baseline>
      <div class="img-6 cover">
        <div class="wrapper join-us-content">
          <div class="h1 red-txt" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-delay="300" data-aos-duration="1000">Join us</div>
          <div class="border red-bg hide-mobile" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-delay="450" data-aos-duration="1000"></div>
          <div class="h1 red-txt" data-aos="fade-up" data-aos-anchor-placement="top-center" data-aos-delay="600" data-aos-duration="1000">on an adventure</div>
        </div>
      </div>
      <button><a href="contact.php">Build our partnership</a></button>
    </section>

    <!-- footer -->
    <footer class="o-container black-bg pt-5">
        <div class="vertical-middle">
            <div class="p-3">
                <p>We collaborate with ambitious brands and people ;<br class="hide-mobile">
                    we’d love to build something great together.</p>
                <button class="btn_white mt-3"><a href="mailto:business@easydistribution.fr">business@easydistribution.fr</a></button>
            </div>
            <div class="logo"><img src="../assets/logo/picto-e.png" alt=""></div>
        </div>
        <!-- Wrapper -->
        <div class="wrapper">
            <!-- First column -->
            <div class="o-third p-3">
              <div class="vertical-middle">
                <h4>Office</h4>
                <p>Toulouse<br>8 rue des Graves<br>France</p>
              </div>
            </div>
            <!-- Second column -->
            <div class="o-third p-3">
              <div class="vertical-middle">
                <h4>Our services</h4>
                <nav>
                  <ul>
                    <li><a href="./whatwedo/selling.php" class="small-border white-txt">Selling</a></li>
                    <li><a href="./whatwedo/logistic.php" class="small-border white-txt">Logistic</a></li>
                    <li><a href="./whatwedo/marketing.php" class="small-border white-txt">Marketing</a></li>
                  </ul>
                </nav>
              </div>
            </div>
            <!-- Third column -->
            <div class="o-third p-3">
              <div class="vertical-middle">
                <h4>Come and say hi</h4>
                <ul>
                  <li><button class="btn_white mt-3"><a href="mailto:david@firstseller.fr">david@firstseller.fr</a></button></li>
                  <li><a class="white-txt" href="tel:+33695141256">+33 5 32 02 47 96</a></li>
                </ul>
              </div>
            </div>
        </div>
        <!-- Terms -->
        <div class="txt-center h10 pb-3 terms">
            <p class="white-txt">Easy Distribution <br class="hide-desktop">©2019 Privacy and Terms</p>
        </div>
    </footer>


</body>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- scripts -->
<script type="text/javascript" src="../js/scripts.js"></script>

</html>
