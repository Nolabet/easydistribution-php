document.addEventListener('DOMContentLoaded', function () {


    let x = window.matchMedia("(max-width: 768px)");
    
    /* CIBLAGE */
    let fleche = document.querySelector('.millieu li img');
    let nav = document.querySelector('nav');
    let nav_bas = document.querySelector('ul.wrapper.bas');
    let red = document.querySelector('.red a');
    let menu = document.querySelector('.menu');
    let menu_img = document.querySelector('.menu img');
    let menu_content = document.querySelector('.millieu');
  
    /* MENU */
    fleche.addEventListener('click', function () {
        nav.classList.toggle('actif');
        nav_bas.classList.toggle('actif');
        fleche.classList.toggle('actif');
        red.classList.toggle('actif');


    });

    if (x.matches) {
        menu.addEventListener('click', function () {
            nav.classList.toggle('active');
            menu_img.classList.toggle('active');
            menu_content.classList.toggle('active');
            nav_bas.classList.toggle('actif');
            fleche.classList.toggle('actif');
        });
    }
});
