<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- What we do -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="icon" type="image/png" href="../../assets/logo/picto-e.png">

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

    <!-- Header -->
    <header class="topnav" id="Navbar">
      <a href="../home.php" class="logo"><img id="logo" src="../../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
      <a href="javascript:void(0);" class="icon" onclick="openNav()">
        <i class="fa fa-bars"></i>
      </a>
      <a href="../contact.php" class="item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
      <a href="../about.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
      <a href="how-to-create-a-brand.php" class="active item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
      <a href="../whatwedo/marketing.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
      <a href="../whatwedo/logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
      <a href="../whatwedo/selling.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
    </header>

    <?php require_once '../partials/menu-js.php'; ?>

    <main>

      <!-- Headings -->
      <section class="blog--header o-container">
        <div>
          <baseline class="black-txt">YOUR QUESTIONS, OUR ANSWERS.</baseline>
          <h1 class="blog--title red-txt mt-2">How to create a brand</h1>
          <p class="blog--intro width68to100 black-txt fix-lh-txt mt-3">Launching your own brand require_onces a rigorous process upstream, from product development to marketing strategy to sales. To ensure a successful launch of your brand, it is essential to go through these essential steps:</p>
        </div>
      </section>


      <!-- Paragraphs -->
      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
              <!-- <i class="fas fa-times-circle"></i> -->
            </span>
            <h4 class="d-inline mb-3">Market research and positioning</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Before launching a product, it is necessary to carry out a market study to see if it is promising. Market research is vital to assess your chances of success by determining: the adequacy of the product with the customers' expectations, the customer segments to be targeted with an adapted strategy, the competition and the existing offers, and to evaluate the opportunities and threats of the sector. Thanks to this study, you can then choose your positioning. This must differentiate you from your competitors and will therefore influence the marketing plan to be put in place. The market research also allows you to define a global strategy according to the sales area and the chosen means of distribution (direct in-store or indirect online) as well as your business model in order to consider how to generate profitability (necessary investments, forecast turnover).</p>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
              <!-- <i class="fas fa-times-circle"></i> -->
            </span>
            <h4 class="d-inline mb-3">Imagine<br class="hide-desktop"> the range</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Do you want a single product range, or different ranges for different segments? This is also the time to choose the key elements of your brand identity: its name and logo. Check that the brand name and/or logo are not already in use. The logo and name must be part of the differentiating elements and easily recognisable.</p>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
              <!-- <i class="fas fa-times-circle"></i> -->
            </span>
            <h4 class="d-inline mb-3">Sourcing</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Essential to determine the places and methods of production thanks to a prospection that must be meticulous in order to find the best supplier who will manufacture the product(s). Prepare a prototype of the product if necessary, so that the manufacturer can get a clear idea of it, and so that you can improve your model more easily. Discussing the project with the manufacturing plant also allows you to determine and negotiate production costs and ensure product compliance for import and export certification.</p>
          </div>
        </div>
      </section>

      <!-- See the following article -->
      <section class="blog--footer mt-5" data-aos="fade-up" data-aos-duration="800">
        <div class="blog--following py-3">
          <h5>SEE THE FOLLOWING ARTICLE</h5>
        </div>
        <div class="blog--next pt-2 width100to75">
          <h4 class="blog--next-title mt-5">HOW TO<br class="hide-desktop"> SELL ONLINE</h4>
          <p class="blog--next-intro w-75 my-3">Nowadays, digital offers many opportunities for business development. But how to sell online? Before starting an e-commerce business, it is essential to take a number of steps to position yourself favourably against your competitors.</p>
          <div class="blog--divider w-100"></div>
          <a href="how-to-sell-online.php" class="a-button"><strong>Read more</strong></a>
        </div>
      </section>

      <!-- <div class="codebar codebar-wwd my-5" data-aos="fade-right" data-aos-duration="1000"></div> -->

    </main>

    <?php require_once '../partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>

    <!-- Menu de navigation du blog -->
    <div class="blog--menu" data-aos="fade-left" data-aos-duration="1500">
      <h6>All articles</h6>
      <ul>
        <li class="reading"><i class="fas fa-book-reader"></i><a href="how-to-create-a-brand.php">How to create a brand?</a></li>
        <li><i class="fas fa-book-open"></i><a href="how-to-sell-online.php">How to sell online?</a></li>
        <li><i class="fas fa-book-open"></i><a href="product-management.php">Product management</a></li>
        <li><i class="fas fa-book-open"></i><a href="why-delegate-your-brand-management.php">Why delegate your brand management?</a></li>
      </ul>
    </div>

</body>

<?php require_once '../partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../../js/scripts.js"></script>
<script type="text/javascript" src="../../js/blog.js"></script>

</html>
