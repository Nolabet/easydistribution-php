#!/bin/bash

# Ajoute le  message de commit
read -p 'Entrez le message de commit : ' message
sudo echo "## $message" >> ~/git/EasyD/easydistribution-php/history.txt

# Copie vers le dépot git
sudo cp -r ./* ~/git/EasyD/easydistribution-php/
sudo chown -R nolab:nolab ./*
