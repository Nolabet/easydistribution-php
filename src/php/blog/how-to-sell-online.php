<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- What we do -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="icon" type="image/png" href="../../assets/logo/picto-e.png">

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

    <!-- Header -->
    <header class="topnav" id="Navbar">
      <a href="../home.php" class="logo"><img id="logo" src="../../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
      <a href="javascript:void(0);" class="icon" onclick="openNav()">
        <i class="fa fa-bars"></i>
      </a>
      <a href="../contact.php" class="item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
      <a href="../about.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
      <a href="how-to-create-a-brand.php" class="active item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
      <a href="../whatwedo/marketing.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
      <a href="../whatwedo/logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
      <a href="../whatwedo/selling.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
    </header>

    <?php require_once '../partials/menu-js.php'; ?>

    <main>

      <!-- Headings -->
      <section class="blog--header o-container">
        <div>
          <baseline class="black-txt">YOUR QUESTIONS, OUR ANSWERS.</baseline>
          <h1 class="blog--title red-txt mt-2">How to sell online?</h1>
          <p class="blog--intro width68to100 black-txt fix-lh-txt mt-3">Nowadays, digital offers many opportunities for business development. But how to sell online? Before starting an e-commerce business, it is essential to take a number of steps to position yourself favourably against your competitors:</p>
        </div>
      </section>


      <!-- Paragraphs -->
      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
            </span>
            <h4 class="d-inline mb-3">Multimedia marketing plan</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Before marketing your products online, it is necessary to create a multimedia marketing plan to enhance your brand and your products.</p>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
            </span>
            <h4 class="d-inline mb-3">Selling online</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>When it comes to distribution channels, it is necessary to communicate about your products. The creation of your own website is therefore an essential step in the growth strategy of your brand. It can also be advantageous to sell your products on marketplaces (Amazon, Cdiscount, EBay...) in addition to your own website. With their millions of visitors per day, they will allow you to have a considerable visibility. It may also be appropriate to create partnerships with retailers depending on the nature of your products in order to extend your reach and thus gain notoriety.</p>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
            </span>
            <h4 class="d-inline mb-3">Visibility</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Improve the visibility of your products thanks to the Community Manager who will best represent your brand by acting as an ambassador. Also focus on quality content by highlighting product sheets and don't hesitate to make advertising placements to help your site get more visibility. Moreover, to access your site, web users will have to enter a query in the Google search bar. It is therefore essential to reference your products using keywords.</p>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
            </span>
            <h4 class="d-inline mb-3">Marketing strategy<br class="hide-desktop"> and advertising</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Before launching into e-commerce, it will also be essential to establish a marketing strategy and to choose the right advertising media if you want to maximise the profitability of your business and achieve your objectives effectively.</p>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
            </span>
            <h4 class="d-inline mb-3">Social networks</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Social networks have become one of the best and most important tools for you to communicate and get closer to your customers. It is therefore impossible today to ignore these platforms in your communication strategy. The distribution of your advertisements on the networks will therefore be a considerable asset enabling you to target your audience in order to communicate with your potential customers according to their centres of interest, particularly via Facebook ads, which are therefore advertisements distributed on Facebook and on partner sites (and applications). Through this system, you can benefit from the power of the most popular social network in the world.</p>
          </div>
        </div>
      </section>

      <!-- See the following article -->
      <section class="blog--footer mt-5" data-aos="fade-up" data-aos-duration="800">
        <div class="blog--following py-3">
          <h5>SEE THE FOLLOWING ARTICLE</h5>
        </div>
        <div class="blog--next pt-2 width100to75">
          <h4 class="blog--next-title mt-5">PRODUCT<br class="hide-desktop"> MANAGEMENT</h4>
          <p class="blog--next-intro w-75 my-3">Logistics is an activity whose purpose is to manage the physical, informational and financial flows of an organisation upstream and downstream, thus making available resources corresponding to the needs, to the economic conditions and for a determined quality of service, in satisfactory conditions of security and safety.</p>
          <div class="blog--divider w-100"></div>
          <a href="product-management.php" class="a-button"><strong>Read more</strong></a>
        </div>
      </section>

    </main>

    <?php require_once '../partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>

    <!-- Menu de navigation du blog -->
    <div class="blog--menu" data-aos="fade-left" data-aos-duration="1500">
      <h6>All articles</h6>
      <ul>
        <li><i class="fas fa-book-open"></i><a href="how-to-create-a-brand.php">How to create a brand?</a></li>
        <li class="reading"><i class="fas fa-book-reader"></i><a href="how-to-sell-online.php">How to sell online?</a></li>
        <li><i class="fas fa-book-open"></i></i><a href="product-management.php">Product management</a></li>
        <li><i class="fas fa-book-open"></i><a href="why-delegate-your-brand-management.php">Why delegate your brand management?</a></li>
      </ul>
    </div>

</body>

<?php require_once '../partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../../js/scripts.js"></script>
<script type="text/javascript" src="../../js/blog.js"></script>

</html>
