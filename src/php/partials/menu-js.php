<!-- Javascript -->
<script>
AOS.init();

/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function openNav() {
  var x = document.getElementById("Navbar");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

function toTheTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

</script>
