#!/bin/bash

# Ajoute le  message de commit
read -p 'Date (jj/mm/aa) : ' d
read -p 'Heure (hh:mm) : ' h
read -p 'Message : ' m
echo "## [$d-$h] $m" >> D:/git/easydistribution-php/history.txt

# Copie vers le dépot git
rm -r -R D:/git/easydistribution-php/src/*
cp -r ~/ D:/git/easydistribution-php/src/
# sudo chown -R nolab:nolab ./*
