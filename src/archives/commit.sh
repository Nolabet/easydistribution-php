#!/bin/bash

# Ajoute le  message de commit
read -p 'Date (jj/mm/aa) : ' d
read -p 'Heure (hh:mm) : ' h
read -p 'Message : ' m
sudo echo "## [$d-$h] $m" >> ~/git/EasyD/easydistribution-php/history.txt

# Copie vers le dépot git
sudo rm -r -R ~/git/EasyD/easydistribution-php/src/*
sudo cp -r ./* ~/git/EasyD/easydistribution-php/src/
sudo chown -R nolab:nolab ./*
