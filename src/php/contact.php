<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- About -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="icon" type="image/png" href="../assets/logo/picto-e.png"/>

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>
    <!-- Header -->
    <header class="topnav" id="Navbar">
      <a href="home.php" class="logo" data-aos="fade-right" data-aos-duration="900"><img id="logo" src="../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
      <a href="javascript:void(0);" class="icon" onclick="openNav()">
        <i class="fa fa-bars"></i>
      </a>
      <a href="contact.php" class="active item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
      <a href="about.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
      <a href="./blog/how-to-create-a-brand.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
      <a href="./whatwedo/marketing.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
      <a href="./whatwedo/logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
      <a href="./whatwedo/selling.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
    </header>

    <?php require_once './partials/menu-js.php'; ?>
    
    <!-- Contact -->
    <section class="wrapper">
      <div class="contact-img o-40 cover" data-aos="fade-right" data-aos-duration="800" id="topContact"></div>
      <div class="o-60 contact-txt relative">
        <div class="white-bg absolute"></div>
        <div class="contact-txt-content">
          <h1 data-aos="fade-left" data-aos-duration="800" data-aos-delay="200" data-aos-anchor="#topContact">Hi, you <br class="hide-desktop">wish to<br>talk to us<br class="hide-desktop">about…</h1>
          <ul class="w-68 red-txt">
              <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="200">pricing</li>
              <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="300">our prestations</li>
              <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="400">or something else</li>
          </ul>
          <p class="black-txt fix-lh-txt" data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">Your contact <span class="red-txt">David Bonavia</span>
          is at your disposal to answer all your questions<br>and talk about
          our future collaboration.</p>
          <div class="my-3">
            <a class="a-button-black mr-3" href="mailto:david@firstseller.fr" data-aos="fade-left" data-aos-duration="800" data-aos-delay="600" data-aos-anchor-placement="top-bottom">Email</a>
            <a class="a-button-black" target="_blank" href="https://wa.me/0033695141256" data-aos="fade-left" data-aos-duration="800" data-aos-delay="700" data-aos-anchor-placement="top-bottom">WhatsApp</a>
          </div>
        </div>
      </div>
    </section>

    <?php require_once './partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>


</body>

<?php require_once './partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../js/scripts.js"></script>

</html>
