<!-- The responsive version is work in progress -->
<div id="wip" class="wip">
  <!-- Modal content -->
  <div class="wip-content">
    <h1>The responsive version is work in progress...</h1>
    <p class="dark-txt mt-5">Please bring a computer with you in order to take full advantage of our website.</p>
    <p class="dark-txt mt-3">Thank you, see you soon!</p>
  </div>
</div>
