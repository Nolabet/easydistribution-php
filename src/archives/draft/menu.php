<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Index -->
    <title>Easy Distribution</title>
    <link rel="stylesheet" href="animation.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/design-system.css">
    <link rel="icon" type="image/png" href="../assets/logo/picto-e.png"/>
</head>

<body>
  <!-- Header -->
  <header class="header">
      <div class="container fixed zindex">
          <nav class="container fixed zindex">
            <div class="w-100" id="Navbar">
              <a href="../index.php" class="logo ml-5"><img id="logo" src="../assets/logo/picto-easyd-red.png" alt=""></a>
              <button type="button" name="button" class="burger m-4 mr-5"><img src="../assets/icons/menu.svg"alt=""></button>
              <ul class="menu pt-2">

                <!-- Menu -->
                <div class="" id="mainNav">
                  <li><a href="home.php">Home</a></li>
                  <li><a id="open_wwd" class="visible">What we do <img class="arrow_menu" src="../assets/icons/fleche-red-down.png" alt=""></a></li>
                  <li><a href="about.php" class="">About</a></li>
                </div>

                <!-- Submenu -->
                <div class=" disabled" id="subNav">
                  <li><a href="./whatwedo/selling.php" class="pl-2">> Selling</a></li>
                  <li><a href="./whatwedo/logistic.php" class="pl-2">> Logistic</a></li>
                  <li><a href="./whatwedo/marketing.php" class="pl-2">> Marketing</a></li>
                </div>

                <button id="contact" class="mr-5"><a href="contact.php" class="red-txt">Contact us</a></button>

              </ul>

            </div>
          </nav>
      </div>
  </header>

</body>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- scripts -->
<script type="text/javascript">

  // Afficher le sous-menu
  function show_subnav() {
    setTimeout(function() {
      console.log("## Show subNav")
      $("#subNav").removeClass("disabled");
      $("#subNav").addClass("shiftnav");
      $("#mainNav").addClass("disabled");
    }, 100);
  }

  // Afficher le sous-menu
  function hide_subnav() {

    setTimeout(function() {
      console.log("## Hide subnav");
      $("#mainNav").removeClass("disabled");
      $("#mainNav").addClass("shiftnav");
      $("#subNav").removeClass("shiftnav");
      $("#subNav").addClass("disabled");
    }, 150);

  }

  var btn = document.getElementById("open_wwd");
  var sub = document.getElementById("subNav");
  btn.addEventListener('mouseover', show_subnav);
  sub.addEventListener('mouseleave', hide_subnav);


</script>

</html>
