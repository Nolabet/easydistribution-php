<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- What we do -->
    <title>Easy Distribution</title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/design-system.css">
    <link rel="icon" type="image/png" href="../assets/logo/picto-e.png" />
</head>

<body>

    <!-- Header -->
    <header class="header">
        <div class="container fixed zindex">
          <nav class="container fixed zindex">
            <div id="mainNav" class="w-100">
              <a href="../index.php" class="logo pl-5"><img id="logo" src="../assets/logo/picto-easyd-red.png" alt=""></a>
              <input class="menu-btn" type="checkbox" id="menu-btn" />
              <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>

              <!-- Menu -->
              <ul class="menu pt-2">
                <li><a href="home.php">Home</a></li>
                <li><a id="liste_click" class="visible">What we do <img class="fleche_menu" src="../assets/icons/fleche-red-down.png" alt=""></a></li>
                <li><a href="about.php">About</a></li>
                <button id="contact"><a href="contact.php" class="red-txt">Contact us</a></button>
              </ul>
            </div>

            <!-- Submenu -->
            <ul id="subNav" class="dropdown-content p-0 mt-0 mx-auto zindex hidden shadow-sm-tn">
              <li class="pl-3"><a class="js-scroll-trigger" href="#toselling"><img src="../assets/icons/picto_selling.png" class="picto" alt="">Selling</a></li>
              <li class="pl-3"><a class="js-scroll-trigger" href="#logistic"><img src="../assets/icons/picto_logistic.png" class="picto" alt="">Logistic</a></li>
              <li class="pl-3"><a class="js-scroll-trigger" href="#marketing"><img src="../assets/icons/picto_marketing.png" class="picto" alt="">Marketing</a></li>
            </ul>

          </nav>
        </div>
    </header>

    <!-- Services -->
    <section class="services">
        <p class="black-txt">OUR SERVICES</p>
        <h1 class="red-txt">This is how you grow <img src="../assets/icons/avion.png" class="avion"></h1>
        <p class="black-txt">All our team will be deicated to helping you export your brand. Its expertise and sharp knowledge will support you during all the process.</p>
        <div class="mt-5" id="toselling"></div>
    </section>


    <!-- Selling -->
    <section id="section2">
        <div class=" o-half left" id="selling">
            <h3 class="w-68 red-txt">
                1. Selling
            </h3>
            <p class="w-68 black-txt">
                Our team is specialized in marketplaces <br>and will know how and where to sell <br>perfectly your products.
            </p>
            <ul class="w-68 red-txt">
                <li> Developpement of a sales strategy effective for France according to your brand image
                </li>
                <li> Adjustements of positioning according to the different partners
                </li>
                <li> Fast and effective cold calling
                </li>
                <li> Negociations with the different partners
                </li>
            </ul>
        </div>
        <img class="img7" src="../assets/img/7.png">
    </section>

    <!-- Logistic -->
    <section class="logistic">
        <img class="img8" src="../assets/img/8.png">
        <div class="o-half" id="logistic">
            <h3 class="red-txt">
                2. Logistic
            </h3>
            <p> We own our private storage bay. Your products will be dispatched easily to all your customers in France and Europe. </p>
            <ul class="red-txt">
                <li> Rent a place in our storage bay at an unbeatable price</li>
                <li> Don't bother about sending your packages, we are taking care of it</li>
                <li> Fast delivery within one or two business days</li>
                <li> Certification of your products to enter and be sold in France <br>and Europe territory</li>
            </ul>
        </div>
    </section>

    <!-- Marketing -->
    <section class="marketing">
        <div class="left o-half" id="marketing">
            <h3 class="red-txt">
                3. Marketing
            </h3>
            <p class="black-txt">
                Our team is specialized in marketing on social media. They'll help you build a strong presence online.
            </p>
            <ul class="red-txt">
                <li> Increase your presence on social media</li>
                <li> Create customer loyalty</li>
                <li> Develop and strengthen your brand image</li>
                <li> Grow your own marketing strategy</li>
            </ul>
        </div>
        <img class="right img9" src="../assets/img/9.png">
    </section>

    <section class="crowd">
        <div>
            <h5 class="black-txt">STAND OUT FROM THE CROWD.</h5>
            <h3 class="red-txt">Be a part of family</h3>
            <a class="red-txt" href="contact.php"><strong>Contact us now</strong></a>
        </div>
    </section>

    <!-- footer -->
    <footer class="o-container black-bg">
        <div class="top relative">
            <div class="logo absolute">
                <img src="../assets/logo/picto-e.png" alt="">
            </div>
            <div class="footer-top-content">
                <p>We collaborate with ambitious brands and people ;<br class="hide-mobile">
                    we’d love to build something great together.</p>
                <button class="btn_white"><a href="mailto:business@easydistribution.fr">business@easydistribution.fr</a></button>
            </div>
        </div>
        <div class="wrapper bottom footer-bottom-content">
            <div>
                <h4>Office</h4>
                <p>Toulouse -<br>8 rue des Graves<br>France</p>
            </div>
            <div>
                <h4>Come and say hi</h4>
                <nav>
                    <ul>
                        <li><button class="btn_white"><a href="mailto:david@firstseller.fr">david@firstseller.fr</a></button></li>
                        <li><a class="white-txt" href="tel:+33695141256">+33 5 32 02 47 96</a></li>
                    </ul>
                </nav>
            </div>

            <div>
                <h4>Our services</h4>
                <nav>
                    <ul>
                        <li><a href="whatwedo.php#selling" class="small-border white-txt">Selling</a></li>
                        <li><a href="whatwedo.php#logistic" class="small-border white-txt">Logistic</a></li>
                        <li><a href="whatwedo.php#marketing" class="small-border white-txt">Marketing</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="txt-center h10 footer-links pb-3">
            <p class="white-txt">Easy Distribution ©2019 Privacy and Terms</p>
        </div>
    </footer>

</body>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>

<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

<!-- scripts -->
<script type="text/javascript" src="../js/scripts.js"></script>
<script type="text/javascript">

//fonction qui gère les menus
function modif_menu() {

    var x = document.getElementsByClassName('container')[0];
    if ($(document.getElementsByClassName("dropdown-content")[0]).hasClass("hidden")) {
        console.log("Dropdown opened");
        //faire apparaitre le sousmenu
        var sousmenu = document.getElementsByClassName("dropdown-content")[0];
        $(sousmenu).removeClass("hidden");
        //changer couleur
        document.getElementById("liste_click").style.color = "#F95B5B";
        //changer img
        var y = document.getElementsByClassName("fleche_menu")[0].setAttribute('src', '../assets/icons//fleche-red-up.png');
        //enlève l'animation de fondu à la fermeture du menu
        $(x).removeClass("animation");

    } else {
        console.log("Dropdown closed");
        //cacher le sousmenu
        var sousmenu = document.getElementsByClassName("dropdown-content")[0];
        $(sousmenu).addClass("hidden");
        //remettre le texte en blanc
        document.getElementById("liste_click").style.color = "white";
        //changer img
        var y = document.getElementsByClassName("fleche_menu")[0].setAttribute('src', '../assets/icons//fleche-red-down.png');
        //rajoute l'animation de fondu à la fermeture du menu
        $(x).addClass("animation");
    }
}

var x = document.getElementById("liste_click");
x.addEventListener('click', modif_menu);

</script>

</html>
