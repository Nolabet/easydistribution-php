<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Preloader -->
    <title>Welcome in Easy Distribution</title>
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/design-system.css">
    <link rel="icon" type="image/png" href="./assets/logo/picto-e.png"/>

</head>

<body>

    <!-- Preloader content -->
    <div id="background" class="">
        <div class="preloader">
            <a href="index.php"><img src="./assets/logo/picto-easyd-red.svg" style="width: 210px"> </a>
            <p class="black-txt">HELLO WE ARE EASYDISTRIBUTION</p>
            <h1 class="red-txt">A caring <br>distribution agency <br> helping you sell <br>your beloved brand<br> across France</h1>
        </div>
        <a href="./php/home" class="bouton_loader fixed" id="bouton_loader">GET STARTED</a>

        <!-- <img id="avion" src="./assets/icons/avion.png" class="fixed zindex"> -->

        <div class="bar_loader fixed rounded-pill"></div>
        <div class="bar_loader fixed hidden rounded-pill" id="bar_loader_2"></div>
    </div>

    <div class="d-none" id="responsive_loader">
      <img src="./assets/logo/picto-easyd-white.svg">
    </div>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="./js/preloader.js"></script>

</body>

</html>
