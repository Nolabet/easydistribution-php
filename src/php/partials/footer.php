<!-- footer rr -->
<footer class="o-container black-bg pt-5">
    <div class="vertical-middle d-grid">
        <div class="p-3">
            <p>We collaborate with ambitious brands and people ;<br class="hide-mobile">
                we’d love to build something great together.</p>
            <!-- <button class="btn_white mt-3"><a href="mailto:business@easydistribution.fr">business@easydistribution.fr</a></button> -->
            <p class="mt-3">See you soon !</p>
        </div>
        <div class="logo"><img src="../assets/logo/picto-e.png" alt=""></div>
    </div>
    <!-- Wrapper -->
    <div class="wrapper">
        <!-- First column -->
        <div class="o-third p-3">
          <div class="vertical-middle">
            <h4>Office</h4>
            <p>Castelginest<br>38 rue des Graves<br>France</p>
          </div>
        </div>
        <!-- Second column -->
        <div class="o-third p-3">
          <div class="vertical-middle">
            <h4>Our services</h4>
            <nav>
              <ul>
                <li><a href="./whatwedo/selling.php" class="small-border white-txt">Selling</a></li>
                <li><a href="./whatwedo/logistic.php" class="small-border white-txt">Logistic</a></li>
                <li><a href="./whatwedo/marketing.php" class="small-border white-txt">Marketing</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- Third column -->
        <div class="o-third p-3">
          <div class="vertical-middle">
            <h4>Come and say hi</h4>
            <div class="d-grid">
              <a class="a-button-white d-inline-block" href="mailto:david@firstseller.fr">david@firstseller.fr</a>
              <a class="white-txt mt-3" href="tel:+33695141256">+33 6 95 14 12 56</a>
            </div>
              <!-- <li><button class="btn_white mt-3"><a href="mailto:david@firstseller.fr">david@firstseller.fr</a></button></li> -->
            </ul>
          </div>
        </div>
    </div>
    <!-- Terms -->
    <div class="txt-center-fixed h10 pb-3 terms">
        <p class="white-txt">Easy Distribution <br class="hide-desktop">©2021 Privacy and Terms</p>
    </div>
</footer>
