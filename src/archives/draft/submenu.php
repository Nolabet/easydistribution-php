<nav class="container fixed zindex">
  <div id="mainNav" class="w-100">
    <a href="../../index.php" class="logo pl-5"><img id="logo" src="../../assets/logo/picto-easyd-red.png" alt=""></a>
    <input class="menu-btn" type="checkbox" id="menu-btn" />
    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>

    <!-- Menu -->
    <ul class="menu pt-2">
      <li><a href="../home.php">Home</a></li>
      <li><a id="liste_click" class="visible">What we do <img class="fleche_menu" src="../../assets/icons/fleche-red-down.png" alt=""></a></li>
      <li><a href="../about.php" class="ml-2">About</a></li>
      <button id="contact"><a href="../contact.php" class="red-txt">Contact us</a></button>
    </ul>
  </div>

  <!-- Submenu -->
  <ul id="subNav" class="dropdown-content p-0 mt-0 mx-auto zindex hidden shadow-sm-tn">
    <li class="pl-3"><a href="../whatwedo.php">> Introduction</a></li>
    <li class="pl-3"><a href="selling.php"><img src="../../assets/icons/picto_selling.png" class="picto" alt="">Selling</a></li>
    <li class="pl-3"><a href="logistic.php"><img src="../../assets/icons/picto_logistic.png" class="picto" alt="">Logistic</a></li>
    <li class="pl-3"><a href="marketing.php"><img src="../../assets/icons/picto_marketing.png" class="picto" alt="">Marketing</a></li>
  </ul>
</nav>

<style media="screen">
/* Dropdown Content */

.dropdown-content{
  height: 10rem;
  width: 10rem;
}

.dropdown-content li a {
    color: #F95B5B !important;
}

.dropdown-content .picto {
  width: 1.2rem;
  height: 1rem;
  padding: 0.3rem;
}
</style>

<script type="text/javascript">

//fonction qui gère les menus
function modif_menu() {

    var x = document.getElementsByClassName('container')[0];
    if ($(document.getElementsByClassName("dropdown-content")[0]).hasClass("hidden")) {
        console.log("Dropdown opened");
        //faire apparaitre le sousmenu
        var sousmenu = document.getElementsByClassName("dropdown-content")[0];
        $(sousmenu).removeClass("hidden");
        //changer couleur
        document.getElementById("liste_click").style.color = "#F95B5B";
        //changer img
        var y = document.getElementsByClassName("fleche_menu")[0].setAttribute('src', '../assets/icons//fleche-red-up.png');
        //enlève l'animation de fondu à la fermeture du menu
        $(x).removeClass("animation");

    } else {
        console.log("Dropdown closed");
        //cacher le sousmenu
        var sousmenu = document.getElementsByClassName("dropdown-content")[0];
        $(sousmenu).addClass("hidden");
        //remettre le texte en blanc
        document.getElementById("liste_click").style.color = "white";
        //changer img
        var y = document.getElementsByClassName("fleche_menu")[0].setAttribute('src', '../assets/icons//fleche-red-down.png');
        //rajoute l'animation de fondu à la fermeture du menu
        $(x).addClass("animation");
    }
}

var x = document.getElementById("liste_click");
x.addEventListener('click', modif_menu);

</script>
