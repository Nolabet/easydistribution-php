/*
  Données
*/
const blog = [ {
  id: 1,
  keywords: [ 'brand', 'Market', 'Sourcing', 'Design', 'Marketing' ],
  title: 'How to create a brand',
  intro: 'Launching your own brand requires a rigorous process upstream, from product development to marketing strategy to sales. To ensure a successful launch of your brand, it is essential to go through these essential steps:',
  firstPart: [
    "Market research and positioning",
    "Before launching a product, it is necessary to carry out a market study to see if it is promising. Market research is vital to assess your chances of success by determining: the adequacy of the product with the customers' expectations, the customer segments to be targeted with an adapted strategy, the competition and the existing offers, and to evaluate the opportunities and threats of the sector. Thanks to this study, you can then choose your positioning. This must differentiate you from your competitors and will therefore influence the marketing plan to be put in place. The market research also allows you to define a global strategy according to the sales area and the chosen means of distribution (direct in-store or indirect online) as well as your business model in order to consider how to generate profitability (necessary investments, forecast turnover)."
  ],
  secondPart: [
    "Imagine the range",
    "Do you want a single product range, or different ranges for different segments? This is also the time to choose the key elements of your brand identity: its name and logo. Check that the brand name and/or logo are not already in use. The logo and name must be part of the differentiating elements and easily recognisable."
  ],
  thirdPart: [
    "Sourcing",
    "Essential to determine the places and methods of production thanks to a prospection that must be meticulous in order to find the best supplier who will manufacture the product(s). Prepare a prototype of the product if necessary, so that the manufacturer can get a clear idea of it, and so that you can improve your model more easily. Discussing the project with the manufacturing plant also allows you to determine and negotiate production costs and ensure product compliance for import and export certification."
  ],
  fourthPart: [
    "Design and marketing",
    "In order to make the brand known, it is important to work on the design aspect to create a unique visual identity. This of course includes the design of packaging, but also of other web and paper communication mediums. In addition to the design, an online marketing plan (communication on social networks, website) and an advertising strategy (online and offline) should be put in place according to the budget."
  ],
  fifthPart: [
    "Production and logistics",
    "The import/export system will need to be determined before production is launched. This includes organising the transport and customs clearance of imports, and discussing with the supplier the distribution of costs and risks during the transport of the products in order to calculate the overall cost of the import operation. When it comes to exporting your products, you can choose to take care of receiving and storing the products and processing the orders yourself, or you can work with a processing centre to ship them. Don't forget to set up a regular inventory to determine the value of the stock in your company and prevent possible management errors."
  ],
  sixthPart: [
    "The sale",
    "Depending on whether you have chosen direct and/or indirect sales, you will need to be rigorous about its set-up, management and follow-up. Before putting your products online, keep in mind that your website should make it easy to find products and that each product should link to others in the same range (or not) so as not to harm the overall visibility of your catalogue. Regarding distribution channels, it can be advantageous to sell your products on marketplaces (Amazon, Ebay) in addition to your own site. With their millions of visitors per day, they will give you considerable visibility. Be careful to manage your orders well to avoid any stock shortage once your activity is launched, and to set up a system of follow-up of your shipments to customers."
  ],
} ]

console.log( blog );
