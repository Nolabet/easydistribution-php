<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- What we do -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../../css/topnav.css">
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="../../css/normalize.css">
    <link rel="stylesheet" href="../../css/design-system.css">
    <link rel="icon" type="image/png" href="../../assets/logo/picto-e.png">

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

      <!-- Header -->
      <header class="topnav" id="Navbar">
        <a href="../home.php" class="logo"><img id="logo" src="../../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
        <a href="javascript:void(0);" class="icon" onclick="openNav()">
          <i class="fa fa-bars"></i>
        </a>
        <a href="../contact.php" class="item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
        <a href="../about.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
        <a href="../blog/how-to-create-a-brand.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
        <a href="marketing.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
        <a href="logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
        <a href="selling.php" class="active item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
      </header>

      <?php require_once '../partials/menu-js.php'; ?>

    <main>
      <!-- Our services -->
      <section class="o-container">
        <div>
          <baseline class="black-txt">OUR SERVICES.</baseline>
          <h1 class="red-txt mt-2">THIS IS HOW WE SELL<img src="../../assets/icons/avion.png" class="avion"></h1>
          <p class="width50to100 w-50 black-txt fix-lh-txt mt-3">All our team will be dedicated to helping you export<br class="hide-mobile">your brand. Its expertise and sharp knowledge will<br class="hide-mobile">support you during all the process.</p>
          <div class="codebar codebar-wwd my-5" data-aos="fade-right" data-aos-duration="1000"></div>
        </div>
      </section>


      <!-- Sale strategy -->
      <section class="o-container wrapper my-5">
        <!-- Text Part -->
        <div class="o-half">
          <!-- Title -->
          <h3 class="red-txt" data-aos="fade-right" data-aos-duration="800" data-aos-delay="300">
            Sale strategy
          </h3>
          <!-- Description -->
          <p class="black-txt fix-lh-txt mt-4" data-aos="fade-right" data-aos-duration="800" data-aos-delay="350">
            Work your strategy to improve your presence<br class="hide-mobile"> and selling, our team support you.
          </p>
          <!-- List items -->
          <ul class="red-txt mt-4">
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="400"> Developpement of a sales strategy effective for France according to your brand image</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="450"> Adjustements of positioning according<br class="hide-mobile"> to the different partners</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="500"> Adapt your strategy to the country you sell</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="550"> improve your presence on each platform</li>
          </ul>
        </div>
        <!-- Img part -->
        <div class="o-half" data-aos="fade-left" data-aos-duration="800" data-aos-delay="600" data-aos-anchor-placement="bottom-bottom">
          <img class="img-wwd-main" src="../../assets/img/16.jpg">
        </div>
      </section>

      <!-- Marketplaces -->
      <section class="o-container wrapper my-5 blue-bg py-5" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
        <!-- Img part -->
        <div class="o-half" data-aos="fade-right" data-aos-duration="800" data-aos-delay="600" data-aos-anchor-placement="bottom-bottom">
          <img class="img-wwd-l" src="../../assets/img/8.png">
        </div>
        <!-- Text Part -->
        <div class="o-half">
          <!-- Title -->
          <h3 class="red-txt" data-aos="fade-left" data-aos-duration="800" data-aos-delay="300">
            Marketplaces
          </h3>
          <!-- Description -->
          <p class="black-txt fix-lh-txt mt-4" data-aos="fade-left" data-aos-duration="800" data-aos-delay="350">
            Our team is speacialized in marketplaces and know how and where to sell perfectly your products.
          </p>
          <!-- List items -->
          <ul class="red-txt mt-4">
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="400">Sell your products on the most famous marketplaces</li>
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="450">Increase your sales</li>
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">Show off your products</li>
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="550">Make your sales easier thaks to the marketplaces<br>and sale in all the countries you want</li>
          </ul>
        </div>
      </section>

      <!-- Negociations -->
      <section class="o-container wrapper my-5">
        <!-- Text Part -->
        <div class="o-half">
          <!-- Title -->
          <h3 class="red-txt" data-aos="fade-right" data-aos-duration="800" data-aos-delay="300">
            Negociations
          </h3>
          <!-- Description -->
          <p class="black-txt fix-lh-txt mt-4" data-aos="fade-right" data-aos-duration="800" data-aos-delay="350">
            Because we want the best for your company,<br class="hide-mobile"> we negociate your prestations to give you the best.
          </p>
          <!-- List items -->
          <ul class="red-txt mt-4">
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="400">We ensure you the best prices for your prestations</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="450">Benefits of exclusive partnerships</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="500">Sell your brand with our best partners</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="550">Get the best prestations</li>
          </ul>
        </div>
        <!-- Img part -->
        <div class="o-half" data-aos="fade-left" data-aos-duration="800" data-aos-delay="600" data-aos-anchor-placement="bottom-bottom">
          <img class="img-wwd-r" src="../../assets/img/9.jpg">
        </div>
      </section>

      <!-- Contact -->
      <div class="py-5 txt-center-fixed w-100" data-aos="fade-up" data-aos-duration="800">
        <h5 class="black-txt mt-5">STAND OUT FROM THE CROWD.</h5>
        <h2 class="red-txt my-3"> Be a part of family</h2>
        <a class="red-txt" href="contact.php"><strong> Contact us now</strong> </a>
      </div>

    </main>

    <?php require_once '../partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>


</body>

<?php require_once '../partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../../js/scripts.js"></script>

</html>
