<!-- WIP -->

<!-- Menu de navigation du blog -->
<div class="blog--menu" data-aos="fade-left" data-aos-duration="1500">
  <h6>All articles</h6>
  <ul>
    <li><i class="fas fa-book-open"></i><a href="how-to-create-a-brand.php">How to create a brand?</a></li>
    <li><i class="fas fa-book-open"></i><a href="how-to-sell-online.php">How to sell online?</a></li>
    <li><i class="fas fa-book-open"></i><a href="product-management.php">Product management</a></li>
    <li class="reading"><i class="fas fa-book-reader"></i><a href="why-delegate-your-brand-management.php">Why delegate your brand management?</a></li>
  </ul>
</div>
