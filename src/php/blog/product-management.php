<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- What we do -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="icon" type="image/png" href="../../assets/logo/picto-e.png">

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

    <!-- Header -->
    <header class="topnav" id="Navbar">
      <a href="../home.php" class="logo"><img id="logo" src="../../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
      <a href="javascript:void(0);" class="icon" onclick="openNav()">
        <i class="fa fa-bars"></i>
      </a>
      <a href="../contact.php" class="item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
      <a href="../about.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
      <a href="how-to-create-a-brand.php" class="active item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
      <a href="../whatwedo/marketing.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
      <a href="../whatwedo/logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
      <a href="../whatwedo/selling.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
    </header>

    <?php require_once '../partials/menu-js.php'; ?>

    <main>

      <!-- Headings -->
      <section class="blog--header o-container">
        <div>
          <baseline class="black-txt">YOUR QUESTIONS, OUR ANSWERS.</baseline>
          <h1 class="blog--title red-txt mt-2">Product management</h1>
          <p class="blog--intro width68to100 black-txt fix-lh-txt mt-3"><strong>What is logistics?</strong></p>
          <p class="blog--intro width68to100 black-txt fix-lh-txt mt-3">Logistics is an activity whose purpose is to manage the physical, informational and financial flows of an organisation upstream and downstream, thus making available resources corresponding to the needs, to the economic conditions and for a determined quality of service, in satisfactory conditions of security and safety.</p>
        </div>
      </section>


      <!-- Paragraphs -->
      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
            </span>
            <h4 class="d-inline mb-3">The stages of logistics</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Logistics comprises several essential steps for good brand management:</p>
            <ul>
              <li>Receipt of goods: the goods are checked to ensure that they are in good condition and then stored;</li>
              <li>Stock management is also a crucial step. Indeed, one of the main frustrations of a consumer is the unavailability of an ordered product. Product management therefore require_onces a maximum of rigour;</li>
              <li>Sending orders: Several possibilities are available to you to send a product. Sending via the network of local shops, using a carrier or withdrawing from your site. International sending is also a great opportunity to send your company to the four corners of the world;/li>
              <li>Returns and after-sales service: anticipating the possible return of goods allows you to be reactive and save time when the moment comes. Thus, good returns management improves loyalty and increases the frequency of purchases and the amount of the average basket;</li>
              <li>The expedition management corresponds to the last link of the logistic chain in the warehouse and should not be neglected. Indeed, the quality of the packaging but above all the delivery experience (respect of the deadline and place, etc...) are key factors of customer satisfaction.</li>
            </ul>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
              <!-- <i class="fas fa-times-circle"></i> -->
            </span>
            <h4 class="d-inline mb-3">Storage brand</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Managing your brand's stock is an important step. Careful consideration should be given to choosing the perfect location for your goods in order not to impact your stock levels. To do this, it is essential to take into account the nature of the goods but also the environmental impact. In order to avoid any risk of alteration of the products and to preserve them in the best possible way, it is therefore necessary to take into account a number of criteria such as temperature, light, humidity, or even the odours of the place...</p>
          </div>
        </div>
      </section>

      <section class="blog--body o-container my-5">
        <div class="blog--1">
          <div class="blog--hook">
            <span class="d-inline mr-2">
              <i class="fas fa-arrow-circle-down"></i>
              <!-- <i class="fas fa-times-circle"></i> -->
            </span>
            <h4 class="d-inline mb-3">Why delegate logistics?</h4>
          </div>
          <div class="blog--text blog--fadeOut width50to100 mt-2">
            <p>Managing a company's supply chain involves high investment and operating costs, but also require_onces expertise in this area. Delegating your brand's logistics to a professional logistics company can therefore be a good alternative. Indeed, these companies, specialised in logistics and storage, benefit from all their know-how, experience and technical skills in this field. They also have warehouses that meet the standards and are suitable for the storage, reception and expedition of your brand and all your cosmetic products. Thanks to their infrastructure and skills, these companies will be able to adapt to any type of storage for your goods in a secure space. Delegating logistics will allow you to benefit from quality services and optimise both your time and your money.</p>
          </div>
        </div>
      </section>

      <!-- See the following article -->
      <section class="blog--footer mt-5" data-aos="fade-up" data-aos-duration="800">
        <div class="blog--following py-3">
          <h5>SEE THE FOLLOWING ARTICLE</h5>
        </div>
        <div class="blog--next pt-2 width100to75">
          <h4 class="blog--next-title mt-5">WHY DELEGATE<br class="hide-desktop"> YOUR BRAND<br class="hide-desktop"> MANAGEMENT</h4>
          <p class="blog--next-intro w-75 my-3">Using external providers means having another company carry out all or part of the goods or services you need or have to provide to your own customers.</p>
          <div class="blog--divider w-100"></div>
          <a href="why-delegate-your-brand-management.php" class="a-button"><strong>Read more</strong></a>
        </div>
      </section>

    </main>

    <?php require_once '../partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>

    <!-- Menu de navigation du blog -->
    <div class="blog--menu" data-aos="fade-left" data-aos-duration="1500">
      <h6>All articles</h6>
      <ul>
        <li><i class="fas fa-book-open"></i><a href="how-to-create-a-brand.php">How to create a brand?</a></li>
        <li><i class="fas fa-book-open"></i><a href="how-to-sell-online.php">How to sell online?</a></li>
        <li class="reading"><i class="fas fa-book-reader"></i></i><a href="product-management.php">Product management</a></li>
        <li><i class="fas fa-book-open"></i><a href="why-delegate-your-brand-management.php">Why delegate your brand management?</a></li>
      </ul>
    </div>


</body>

<?php require_once '../partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../../js/scripts.js"></script>
<script type="text/javascript" src="../../js/blog.js"></script>

</html>
