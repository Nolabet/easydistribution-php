<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- What we do -->
    <title>Easy Distribution</title>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="../../css/topnav.css">
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="../../css/normalize.css">
    <link rel="stylesheet" href="../../css/design-system.css">
    <link rel="icon" type="image/png" href="../../assets/logo/picto-e.png">

    <!-- Animate On scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>

    <!-- Header -->
    <header class="topnav" id="Navbar">
      <a href="../home.php" class="logo"><img id="logo" src="../../assets/logo/picto-easyd-red.svg" alt="logo" style="width: 20vh;"></a>
      <a href="javascript:void(0);" class="icon" onclick="openNav()">
        <i class="fa fa-bars"></i>
      </a>
      <a href="../contact.php" class="item contact" data-aos="fade-left" data-aos-duration="1250" >Contact</a>
      <a href="../about.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> About</a>
      <a href="../blog/how-to-create-a-brand.php" class="item" data-aos="fade-left" data-aos-duration="1150" >> Blog</a>
      <a href="marketing.php" class="active item tab-wwd" data-aos="fade-left" data-aos-duration="1100" >> Marketing</a>
      <a href="logistic.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1050" >> Logistic</a>
      <a href="selling.php" class="item tab-wwd" data-aos="fade-left" data-aos-duration="1000" >> Selling</a>
    </header>

    <?php require_once '../partials/menu-js.php'; ?>

    <main>
      <!-- Our services -->
      <section class="o-container">
        <div>
          <baseline class="black-txt">OUR SERVICES.</baseline>
          <h1 class="red-txt mt-2">THIS IS HOW WE MARKET<img src="../../assets/icons/avion.png" class="avion"></h1>
          <p class="width50to100 w-50 black-txt fix-lh-txt mt-3">our team of experts will support you in defining your digital acquisition strategy and optimizing your visibility<br class="hide-mobile"> for more results.</p>
          <div class="codebar codebar-wwd my-5" data-aos="fade-right" data-aos-duration="1000"></div>
        </div>
      </section>


      <!-- Listening is the key -->
      <section class="o-container wrapper my-5">
        <!-- Text Part -->
        <div class="o-half">
          <!-- Title -->
          <h3 class="red-txt" data-aos="fade-right" data-aos-duration="800" data-aos-delay="300">
            Listening is the key
          </h3>
          <!-- Description -->
          <p class="black-txt fix-lh-txt mt-4" class="black-txt fix-lh-txt mt-4" data-aos="fade-right" data-aos-duration="800" data-aos-delay="350">
            Because you know what you want, our team is here to listen to you match with your ambitions.
          </p>
          <!-- List items -->
          <ul class="red-txt mt-4">
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="400">Determine your objectives, create a marketing plan to reach it and built solid strategy</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="450">A collective work where you are the main actor</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="500">Your advice is important, you are the key of your success</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="550">A durable partnership for all your projects</li>
          </ul>
        </div>
        <!-- Img part -->
        <div class="o-half" data-aos="fade-left" data-aos-duration="800" data-aos-delay="600" data-aos-anchor-placement="bottom-bottom">
          <img class="img-wwd-main" src="../../assets/img/24.jpg">
        </div>
      </section>

      <!-- An amazing team for you -->
      <section class="o-container wrapper my-5 blue-bg py-5"  data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
        <!-- Img part -->
        <div class="o-half"  data-aos="fade-right" data-aos-duration="800" data-aos-delay="600" data-aos-anchor-placement="bottom-bottom">
          <img class="img-wwd-l" src="../../assets/img/26.jpg">
        </div>
        <!-- Text Part -->
        <div class="o-half">
          <!-- Title -->
          <h3 class="red-txt" data-aos="fade-left" data-aos-duration="800" data-aos-delay="300">
            An amazing team for you
          </h3>
          <!-- Description -->
          <p class="black-txt fix-lh-txt mt-4" data-aos="fade-left" data-aos-duration="800" data-aos-delay="350">
            All our team is dedicated to your project to increase your presence and promote you brand.
          </p>
          <!-- List items -->
          <ul class="red-txt mt-4">
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="400">A disposable team to support your developpement</li>
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="450">A complete follow of your project</li>
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="500">The best advices adapted to your brand</li>
            <li data-aos="fade-left" data-aos-duration="800" data-aos-delay="550">A unique expertise to bring up your project</li>
          </ul>
        </div>
      </section>

      <!-- Marketing -->
      <section class="o-container wrapper my-5">
        <!-- Text Part -->
        <div class="o-half">
          <!-- Title -->
          <h3 class="red-txt" data-aos="fade-right" data-aos-duration="800" data-aos-delay="300">
            Marketing
          </h3>
          <!-- Description -->
          <p class="black-txt fix-lh-txt mt-4" data-aos="fade-right" data-aos-duration="800" data-aos-delay="350" data-aos="fade-right" data-aos-duration="800" data-aos-delay="400">
            Because you deserve the best we support you all along the marketing process to improve its visibility.
          </p>
          <!-- List items -->
          <ul class="red-txt mt-4">
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="400">Increase your presence on social media</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="450">Create customer loyalty</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="500">Develop and strengthen your brand image</li>
            <li data-aos="fade-right" data-aos-duration="800" data-aos-delay="550">Grow your own marketing strategy</li>
          </ul>
        </div>
        <!-- Img part -->
        <div class="o-half">
          <img class="img-wwd-r" src="../../assets/img/9.jpg">
        </div>
      </section>

      <!-- Contact -->
      <div class="py-5 txt-center-fixed w-100" data-aos="fade-up" data-aos-duration="800">
        <h5 class="black-txt mt-5">STAND OUT FROM THE CROWD.</h5>
        <h2 class="red-txt my-3"> Be a part of family</h2>
        <a class="red-txt" href="contact.php"><strong> Contact us now</strong> </a>
      </div>

    </main>

    <?php require_once '../partials/footer.php'; ?>

    <a onclick="toTheTop();" id="anchor" title="Go to top" data-aos="fade-up"><i class="fas fa-angle-up"></i></a>

</body>

<?php require_once '../partials/libraries.php'; ?>

<!-- scripts -->
<script type="text/javascript" src="../../js/scripts.js"></script>

</html>
