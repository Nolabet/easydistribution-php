(function($) {

  let x = window.matchMedia("(min-width: 960px)");
  let home = 'https://easydistribution.fr/php/home.php'

  // For laptop version
    if (x.matches) { // If media query matches
    console.log("#1 Fade in");
    setTimeout(function() {
      $("#bar_loader_2").removeClass("hidden");
      $('#avion').animate({
        left: "+=79.5%"
      }, 3000);
      setTimeout(function() {
        $('#bouton_loader').css({
          "opacity": "100%",
          "cursor": "pointer",
          "pointer-events": "auto"
        });
        console.log("#3 Get started !");
        redirectToAnotherPage(home, 1500);
      }, 3000);
      console.log("#2 Loading");
    }, 4000);

  } else {
    $("#background").addClass("d-none");
    $("#responsive_loader").removeClass("d-none");
    redirectToAnotherPage(home, 3000);
  };

function redirectToAnotherPage(page, timeout){
  setTimeout(function() {
        // get navigator metadata
        var ua      = navigator.userAgent.toLowerCase(),
            isIE    = ua.indexOf('msie') !== -1,
            version = parseInt(ua.substr(4, 2), 10);

        // window.navigate; ONLY for old versions of Internet Explorer
        if (isIE && version < 9) {
          window.navigate('top.jsp');
        } else {
          window.location.href = page;
        }
    }, timeout);
}

// window.navigate; ONLY for old versions of Internet Explorer
// var link = document.createElement('a');
// link.href = url;
// document.body.appendChild(link);
// link.click();

// window.location
// window.location.replace('http://www.example.com')
// window.location.assign('http://www.example.com')
// window.location.href = 'http://www.example.com'
// document.location.href = '/path'

// window.history
// window.history.back()
// window.history.go(-1)

// jQuery
// $(location).attr('href','http://www.example.com')
// $(window).attr('location','http://www.example.com')
// $(location).prop('href', 'http://www.example.com')

})(jQuery); // End of use strict
